//
//  **************************************************************
//  * The simple shashlik-type detector for synchotron radiation *
//  **************************************************************

#include "SRdetDetectorConstruction.hh"
#include "SRdetActionInitialization.hh"
#include "PhysicsList.hh"

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4UIcommand.hh"
#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"

#include "Randomize.hh"

#include "SRdetUSets.hh"


int main (int argc, char** argv) {
    
    G4String macro;
    G4String session;   

    if ( argc > 15 ) {
        SRdetUSets::Instance()->PrintUsage();
        return 1;
    }
    
    //  Parsing arguments
    for ( G4int i = 1; i<argc; i+=2 ) {
        if      ( G4String(argv[i]) == "-e" ) SRdetUSets::Instance()->SetSREnergy( G4UIcommand::ConvertToDouble(argv[i+1]) );
        else if ( G4String(argv[i]) == "-t" ) SRdetUSets::Instance()->SetPbThickness( G4UIcommand::ConvertToDouble(argv[i+1]) );
        else if ( G4String(argv[i]) == "-l" ) SRdetUSets::Instance()->SetNLayers( G4UIcommand::ConvertToInt(argv[i+1]) );
        else if ( G4String(argv[i]) == "-x" ) SRdetUSets::Instance()->SetXpos( G4UIcommand::ConvertToDouble(argv[i+1]) );
        else if ( G4String(argv[i]) == "-b" ) SRdetUSets::Instance()->SetBeamType( argv[i+1] );
        else if ( G4String(argv[i]) == "-m" ) macro = argv[i+1];
        else if ( G4String(argv[i]) == "-s" ) session = argv[i+1];       
        else SRdetUSets::Instance()->PrintUsage();
    }

    //  Detect interactive mode (if no macro provided) and define UI session
    G4UIExecutive* ui = 0;
    if ( ! macro.size() ) {
    ui = new G4UIExecutive( argc, argv );    
    }

    // Choose the random engine
    G4Random::setTheEngine( new CLHEP::RanecuEngine );

    //  Construct the deafault run manager
    G4RunManager * runManager = new G4RunManager;

    //  Set mandatory initialization classes 
    SRdetDetectorConstruction* detectorConstruction = new SRdetDetectorConstruction();
    runManager->SetUserInitialization(detectorConstruction);

    runManager->SetUserInitialization(new PhysicsList);
    
    SRdetActionInitialization* actionInitialization = new SRdetActionInitialization();
    runManager->SetUserInitialization(actionInitialization);

    // Initialize visualization
    G4VisManager* visManager = new G4VisExecutive;

    // G4VisExecutive can take a verbosity argument - see /vis/verbose guidance.
    // G4VisManager* visManager = new G4VisExecutive("Quiet");
    visManager->Initialize();
 
    //  Get the pointer to the User Interface manager
    G4UImanager* UImanager = G4UImanager::GetUIpointer();

    //  Process macro or start UI session
    if ( macro.size() ) {
        //  batch mode
        G4String command = "/control/execute ";
        UImanager->ApplyCommand(command+macro);
    }
    else {
        // interactive mode
        UImanager->ApplyCommand("/control/execute init_vis.mac");
        //  Maybe GUI should be done? TODO if (ui->IsGUI()) {
        //   UImanager->ApplyCommand("/control/execute gui.mac");
        //}
        ui->SessionStart();
    delete ui;
    }

    //  Job termination
    //  Free the store: user actions, physics_list and detector_description are
    //  owned and deleted by the run manager, so they should not be deleted 
    //  in the main() program !
    
    delete visManager;
    delete runManager;


} 
