#include "HistoMan.hh"
#include "SRdetUSets.hh"

#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

HistoMan::HistoMan() : FileName("SRdet")
{
    Book();
}

HistoMan::~HistoMan()
{
    delete G4AnalysisManager::Instance();
}

void HistoMan::Book()
{
    //  Create or get Analysis manager
    G4AnalysisManager* aMan = G4AnalysisManager::Instance();
    aMan->SetFileName(FileName);
    aMan->SetVerboseLevel(1);
    aMan->SetActivation(true);  //  enable activation
    aMan->SetFirstHistoId(1);
    
    //  Define histos
    G4String ScString = "Energy deposit in Shashlik";
 
    //  Create histos for each shashlik and total
    for ( G4int i=0; i < SRdetUSets::Instance()->GetNofCellsX()*
                       SRdetUSets::Instance()->GetNofCellsY(); i++)
    {   
        G4String nameSc, titleSc;
        titleSc = ScString + std::to_string(i+1);
        nameSc = std::to_string(i+1);
                             
        aMan->CreateH1(nameSc, titleSc, 1000, 0., 100*MeV);
        //  TODO  aMan->SetH1Activation(ih, false);
    }
    
    //  ---Histos for each shashliks---Total energy deposition in all shashliks,SR photons spectrum, Power spectrum of SR photons
    aMan->CreateH1("TotSc", "Total Edep in Sc", 1000, 0., 100*MeV);
    
    aMan->CreateH1("PhotSpectrum", "Spectrum of synchrotron photons", 1000, 0., 100 );

    aMan->CreateH1("PowerPhotSpectrum", "Power spectrum of synchrotron photons", 1000, 0., 100);

}












