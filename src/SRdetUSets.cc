#include "SRdetUSets.hh"

SRdetUSets* SRdetUSets::_self = nullptr;

SRdetUSets::SRdetUSets() :
PbThickness_mm (0.5),
SREnergy_mev (1.0),
NLayers (200),
Xpos(0.),
NofCellsX(8),
NofCellsY(4),
PGunPos (0),
BeamType ("uniform")
{
}

SRdetUSets::~SRdetUSets()
{
}

SRdetUSets* SRdetUSets::Instance()
{
    if ( ! _self )
        {
            _self = new SRdetUSets();
        }
    return _self;
}

void SRdetUSets::SetPbThickness( G4double tValue )
{
    PbThickness_mm = tValue;
}

void SRdetUSets::SetSREnergy( G4double tValue)
{
    SREnergy_mev = tValue;
}

void SRdetUSets::SetNLayers( G4int tValue )
{
    NLayers = tValue;
}

void SRdetUSets::SetPGunPos( G4double tValue )
{
    PGunPos = tValue;
}

void SRdetUSets::SetXpos( G4double tValue )
{
    Xpos = tValue;
}

void SRdetUSets::SetNofCellsX( G4int tValue )
{
    NofCellsX = tValue;
}

void SRdetUSets::SetNofCellsY( G4int tValue )
{
    NofCellsY = tValue;
}

void SRdetUSets::SetBeamType( G4String tBeamType )
{
    BeamType = tBeamType;
}

void SRdetUSets::PrintUsage()
{
    G4cerr << " Usage:" << G4endl;
    G4cerr << " SRdet [-e energy of photon] [-t thickness of Pb layer] [-l number of layers]" 
           << " [-x X coordinate of Shashlik's center] [-u UIsession] [-m macro]"
           << G4endl 
           << " [-b Beam type(noDistribution, uniform(default) are available)]"
           << G4endl;
}   
