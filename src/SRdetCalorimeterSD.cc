#include "SRdetCalorimeterSD.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"

#include "SRdetUSets.hh"

#include <iostream>  //  XXX

SRdetCalorimeterSD::SRdetCalorimeterSD( const G4String& name,
                                        const G4String& hitsCollectionName)                                        
    : G4VSensitiveDetector(name),
      HitsCollection(0)
{
    collectionName.insert(hitsCollectionName);
}

SRdetCalorimeterSD::~SRdetCalorimeterSD() 
{
}

void SRdetCalorimeterSD::Initialize(G4HCofThisEvent* hce) 
{
     
    //  Create Hit cillection
    HitsCollection = new SRdetCalHitsCollection(SensitiveDetectorName, collectionName[0]);
    
    //  Add collection in hce
    G4int hceID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
    hce->AddHitsCollection( hceID, HitsCollection);
    
    //  Create Hits
    // number of shashliks + 1 ()
    //        for example Matrix 4*4 has 16 + 1 Hits 
    G4int NofShashliks = SRdetUSets::Instance()->GetNofCellsX() * SRdetUSets::Instance()->GetNofCellsY();
    for ( G4int i = 0; i < NofShashliks + 1; i++) 
    {
        HitsCollection->insert( new SRdetCalorHit() );
    }
    
}

G4bool SRdetCalorimeterSD::ProcessHits( G4Step* step, G4TouchableHistory*) 
{
    //  energy deposition
    
    G4double edep = step->GetTotalEnergyDeposit();

    G4double StepLength = 0.;
    if ( step->GetTrack()->GetDefinition()->GetPDGCharge() != 0 )
    {
        StepLength = step->GetStepLength();
    }

    if ( edep==0 && StepLength == 0) return false;
    
    const G4TouchableHistory* touchable = dynamic_cast<const G4TouchableHistory*> (step->GetPreStepPoint()->GetTouchable());

    //  Get coordinates of shashlik:

    G4int Ycoord = touchable->GetReplicaNumber(3);  //  VERY IMPORTANT  
    G4int Xcoord = touchable->GetReplicaNumber(2);
    
    G4int XYcoord = Ycoord*SRdetUSets::Instance()->GetNofCellsX() + Xcoord;  //  Coordinates of matrix X*Y convert to number in one dimension HitsCollection
    
    //  Get hit accounting data for shashlik
    SRdetCalorHit* HitTotalXY = (*HitsCollection)[XYcoord];
    if ( !HitTotalXY )
    {
        G4ExceptionDescription msg;
        msg << "Can not acces hit" << XYcoord;
        G4Exception("SRdetCalorimeterSD::ProcessHits()","MyCode004", FatalException, msg);
    }
    
    //  Get hit for total calorimeter accounting
    SRdetCalorHit* HitTotalCal = (*HitsCollection)[HitsCollection->entries() - 1];

    //  Add values
    HitTotalXY->Add(edep);
    HitTotalCal->Add(edep);

    return true;    
}

void SRdetCalorimeterSD::EndOfEvent(G4HCofThisEvent*)
{
   
    if ( verboseLevel>1 ) { 
       G4int nofHits = HitsCollection->entries();
       G4cout
           << G4endl 
           << "-------->Hits Collection: in this event there are " << nofHits 
           << " hits in the calorimeter: " << G4endl;
       for ( G4int i=0; i<nofHits; i++ ) (*HitsCollection)[i]->Print();
  }

}




