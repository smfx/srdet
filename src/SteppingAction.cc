#include "SteppingAction.hh"
#include "HistoMan.hh"
#include "SRdetUSets.hh"

#include "G4SteppingManager.hh"
#include "G4VProcess.hh"
#include "G4ParticleTypes.hh"
#include "G4UnitsTable.hh"
#include "G4EmProcessSubType.hh"
#include "G4SystemOfUnits.hh"

#include <iostream>  //  XXX

SteppingAction::SteppingAction() : G4UserSteppingAction()
{}

SteppingAction::~SteppingAction()
{}

void SteppingAction::UserSteppingAction( const G4Step* tStep)
{   
    const G4VProcess* process = tStep->GetPostStepPoint()->GetProcessDefinedStep();
    if ( process == 0 ) { return; }
        
    if ( process->GetProcessSubType() == fSynchrotronRadiation )
    {
        //  XXX? const G4StepPoint* PrePoint = tStep->GetPreStepPoint();
        const G4TrackVector* secondary = fpSteppingManager->GetSecondary();
            
        //  In geant4 (*secondary)[lp-1] points to the last generated photon
        size_t lp = (*secondary).size();
        if ( lp )
        {
            G4double Egamma = (*secondary)[lp-1]->GetTotalEnergy();
            if (Egamma!=0)      //  XXX
            {
                G4AnalysisManager* aMan = G4AnalysisManager::Instance();

                aMan->FillH1( SRdetUSets::Instance()->GetNofCellsX()*
                          SRdetUSets::Instance()->GetNofCellsY() + 2, Egamma );                     //  It's miserable way to handle histos.
                                                                                                    //  Especially bad is presence of magick numbers 2 and 3.
                //  Power spectrum. Energy of SR photon are used as weight.                         //  It should be reworked.
                aMan->FillH1( SRdetUSets::Instance()->GetNofCellsX()*                               //
                              SRdetUSets::Instance()->GetNofCellsY() + 3, Egamma, Egamma/MeV );     //                 
            }
        }
    }
}
