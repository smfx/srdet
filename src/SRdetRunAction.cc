#include "SRdetRunAction.hh"
#include "HistoMan.hh"
#include "SRdetUSets.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"

#include <stdio.h>
#include <iostream>

SRdetRunAction::SRdetRunAction() : G4UserRunAction(),
hMan(0)
{
    hMan = new HistoMan();   
}

SRdetRunAction::~SRdetRunAction()
{
    delete G4AnalysisManager::Instance();
    delete hMan;
}

void SRdetRunAction::BeginOfRunAction(const G4Run* /*run*/)
{ 
    // Get analysis manager
    G4AnalysisManager* aMan = G4AnalysisManager::Instance();

    // Open an output file
    //
    if ( aMan->IsActive() )
    {
        aMan->OpenFile();
    }
}

void SRdetRunAction::EndOfRunAction(const G4Run*)
{
    G4AnalysisManager* aMan = G4AnalysisManager::Instance();
    if ( aMan->IsActive() )
    {
        aMan->Write();
        aMan->CloseFile();
    }
}


