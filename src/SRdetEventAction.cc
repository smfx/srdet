#include "SRdetEventAction.hh"
#include "SRdetUSets.hh"
#include "HistoMan.hh"

#include "G4RunManager.hh"
#include "G4SDManager.hh"
#include "G4UnitsTable.hh"
#include "G4HCofThisEvent.hh"
#include "G4Event.hh"

#include "Randomize.hh"
#include <iomanip>

#include <iostream>

SRdetEventAction::SRdetEventAction()
    : G4UserEventAction(),
        ScHCEID(-1)
{}

SRdetEventAction::~SRdetEventAction()
{}

SRdetCalHitsCollection* SRdetEventAction::GetHitsCollection( G4int hcID,
                                                             const G4Event* event) const
{
    SRdetCalHitsCollection*  HitsCollection = dynamic_cast<SRdetCalHitsCollection*>(
                                                           event->GetHCofThisEvent()->GetHC(hcID) );
    if ( ! HitsCollection ) 
    {
        G4ExceptionDescription msg;
        msg << "Cannot access HitsCollection ID " << hcID; 
        G4Exception("SRdetEventAction::GetHitsCollection()",
        "MyCode0003", FatalException, msg);
    }  

    return HitsCollection;

}

void SRdetEventAction::PrintEventStatistics(
                              G4double ScEdep) const
{
  // print event statistics
  G4cout
     << "   Scintillator: total energy: " 
     << std::setw(7) << G4BestUnit(ScEdep, "Energy")
     << G4endl;
}

void SRdetEventAction::BeginOfEventAction(const G4Event*)
{}

void SRdetEventAction::EndOfEventAction(const G4Event* event)
{
    //  Get hits collections IDs (only once)
    if ( ScHCEID==-1 )
    {
        ScHCEID = G4SDManager::GetSDMpointer()->GetCollectionID("ScintillatorHitsCollection");
    }

    //  Get hits collection
    SRdetCalHitsCollection* ScHC = GetHitsCollection(ScHCEID, event);

    G4AnalysisManager* aMan = G4AnalysisManager::Instance();
    //  Get hits for every shashliks and total and fill histos for Sc layers
    
    for ( G4int i = 0; i < ScHC->entries(); i++)
    {
        if ( (*ScHC)[i]->GetEdep() ) 
        {   
            aMan->FillH1(i+1, (*ScHC)[i]->GetEdep() );
        }
    }

}
