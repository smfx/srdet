#include "SRdetCalorHit.hh"

#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"

#include <iomanip>

SRdetCalorHit::SRdetCalorHit() : G4VHit(),
    Edep(0)
{}

SRdetCalorHit::~SRdetCalorHit()
{}

SRdetCalorHit::SRdetCalorHit(const SRdetCalorHit& original) : G4VHit()
{
    Edep = original.Edep;
    }

const SRdetCalorHit& SRdetCalorHit::operator=(const SRdetCalorHit& original)
{
    Edep = original.Edep;
    
    return *this;
}

G4int SRdetCalorHit::operator==(const SRdetCalorHit& original) const
{
    return ( this == &original ) ? 1 : 0;
}

void SRdetCalorHit::Print()
{
    G4cout
        << "Edep: "
        << std::setw(7) << G4BestUnit(Edep, "Energy")
        << G4endl;
}





