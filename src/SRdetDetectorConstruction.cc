#include "SRdetUSets.hh"
#include "SRdetDetectorConstruction.hh"
#include "SRdetCalorimeterSD.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4AutoDelete.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4TransportationManager.hh"
#include "MagneticField.hh"
#include "G4SDManager.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4UniformMagField.hh"

#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

#include <stdlib.h>  //  abs
#include <iostream>

//  The longitudal distance of sandwich from its center to center of layer(i -- layers before, j -- after)
//  d_n = ( \sum_{i < n} w_i - \sum_{j > n} w_j )/2

G4ThreadLocal 
G4FieldManager* SRdetDetectorConstruction::FieldMan = 0;

G4ThreadLocal
MagneticField* SRdetDetectorConstruction::MagField = 0; 


SRdetDetectorConstruction::SRdetDetectorConstruction()
    : G4VUserDetectorConstruction(),
    CheckOverlaps(true) 
{
}

SRdetDetectorConstruction::~SRdetDetectorConstruction()
{
}

G4VPhysicalVolume* SRdetDetectorConstruction::Construct() {
    DefineMaterials();
    return DefineVolumes();
}

void SRdetDetectorConstruction::DefineMaterials() {
    
    G4double density, a;
    G4String name;
    G4int z;
  
    G4NistManager* nistManager = G4NistManager::Instance();
    
    nistManager->FindOrBuildMaterial("G4_Pb");
    nistManager->FindOrBuildMaterial("G4_H");
    nistManager->FindOrBuildMaterial("G4_C");
    nistManager->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE");
        
    //  Vacuum
    new G4Material("Galactic", z=1, a=1.01*g/mole, density=universe_mean_density,
                    kStateGas, 2.73*kelvin, 3.e-18*pascal);

    G4cout << *(G4Material::GetMaterialTable()) << G4endl;

}

G4VPhysicalVolume* SRdetDetectorConstruction::DefineVolumes() {
    
    //  Geometry
    G4int nCellsX = 8;
    G4int nCellsY = 4;
    //  Dimensions of the tube with magnetic field
    G4double InRad_mm = 0*mm;
    G4double OutRad_mm = 400*mm;
    G4double TubeHz_m = 2*m;
    //  ------------------------------------------

    SRdetUSets::Instance()->SetNofCellsX( nCellsX );
    SRdetUSets::Instance()->SetNofCellsY( nCellsY );

    G4int NLayers = SRdetUSets::Instance()->GetNLayers();
      
    G4double Pb_mm = SRdetUSets::Instance()->GetPbThickness()*mm;
    G4double Sc_mm = 1.5*mm;
    G4double CellSizeXY_mm = 38.2*mm;
    
    G4double Layer_mm = Sc_mm + Pb_mm;
    G4double CalorimeterZ_mm = Layer_mm * NLayers;

    G4double CalorimeterX_mm = CellSizeXY_mm * nCellsX;
    G4double CalorimeterY_mm = CellSizeXY_mm * nCellsY;
    
    //  Positions
    G4double distance_m = 17.5*m;  //  Distance between magnet and calorimeter 

    //  Calorimeter position
    G4double CalorPosX = SRdetUSets::Instance()->GetXpos()*cm;
    G4double CalorPosY = 0.1*cm;                                    //  Its most dark magick in whole project. The ordinary Y=0 dont work properly with
                                                                    //  some Pb thickness.
                                                                    //  I really dont like it.
    G4double CalorPosZ = (TubeHz_m + distance_m)/2;

    //  Magnet tube position
    G4double TubePos = -(distance_m + CalorimeterZ_mm)/2;    
    
    //  World geometrical dimensions--------------------------------------
    
    G4double maxX = 0;
    OutRad_mm > (CalorimeterX_mm + abs(CalorPosX)) ? maxX = 2*OutRad_mm : maxX = 2*(CalorimeterX_mm + abs(CalorPosX)); 
    
    G4double maxY = 0;
    OutRad_mm > (CalorimeterY_mm + abs(CalorPosY)) ? maxY = 2*OutRad_mm : maxY = 2*(CalorimeterY_mm + abs(CalorPosY));
    
    G4double WorldSizeX_mm = 1.2 * maxX;
    G4double WorldSizeY_mm = 1.2 * maxY;
    G4double WorldSizeZ_mm = 1.2 * (CalorimeterZ_mm + distance_m + TubeHz_m);
    //  ------------------------------------------------------------------

    SRdetUSets::Instance()->SetPGunPos( WorldSizeZ_mm/2 );  //  Define the gun position
    
    //  Get materials
    G4Material* defaultMaterial = G4Material::GetMaterial("Galactic");
    G4Material* AbsorberMaterial = G4Material::GetMaterial("G4_Pb");
    G4Material* ScMaterial = G4Material::GetMaterial("G4_PLASTIC_SC_VINYLTOLUENE");
  
    if ( ! defaultMaterial || ! AbsorberMaterial || ! ScMaterial ) {
        G4ExceptionDescription msg;
        msg << "Cannot retrieve materials already defined."; 
        G4Exception("B4DetectorConstruction::DefineVolumes()",
        "MyCode0001", FatalException, msg);
    }

    //  WORLD
    G4VSolid* WorldS = new G4Box (
        "World",                                                //  Name
        WorldSizeX_mm/2, WorldSizeY_mm/2, WorldSizeZ_mm/2);   //  Dimensions

    G4LogicalVolume* WorldLV = new G4LogicalVolume (
        WorldS,             //  It's solid volume
        defaultMaterial,    //  It's material
        "World");           //  It's name

    G4VPhysicalVolume* WorldPV = new G4PVPlacement (
        0,                  //  no rotation
        G4ThreeVector(),    //  at (0, 0, 0)
        WorldLV,            //  it's Logical Volume
        "World",            //  it's name
        0,                  //  it's mother volume
        false,              //  no boolean operation
        0,                  //  number copy
        CheckOverlaps);

    //  Tube with local magnetic field-------------------------------------------------------------------
    G4VSolid* MagneticS = new G4Tubs ("MagneticTube", InRad_mm, OutRad_mm, TubeHz_m/2, 0.*deg, 360*deg);

    MagneticLV = new G4LogicalVolume(MagneticS, defaultMaterial, "MagneticLogical");
    
    //  Placement of this tube
    new G4PVPlacement ( 0,  //  Rotation
                        G4ThreeVector(0., 0., TubePos),
                        MagneticLV,
                        "MagneticPhysical",  //  Name
                        WorldLV,  //  Mother
                        false,   //  boolean opeartion
                        0,
                        CheckOverlaps);
    //  -------------------------------------------------------------------------------------------------

    //  CALORIMETER
    G4VSolid* CalorimeterS = new G4Box (
        "Calorimeter",
        CalorimeterX_mm/2, CalorimeterY_mm/2, CalorimeterZ_mm/2);

    G4LogicalVolume* CalorimeterLV = new G4LogicalVolume (
        CalorimeterS,
        defaultMaterial,
        "Calorimeter");
    
    //  Define rotation  //  TODO  Probably we dont need rotation
    //  G4RotationMatrix* Rot = new G4RotationMatrix();
    //  Rot->rotateY(30.*deg);
    new G4PVPlacement (
        0,
        G4ThreeVector( CalorPosX, CalorPosY, CalorPosZ),
        CalorimeterLV,
        "Calorimeter",
        WorldLV,
        false,
        0,
        CheckOverlaps);

    //  Vertical CALORIMETER layer
    G4VSolid* VertLayerS = new G4Box (
        "VertLayer",
        CalorimeterX_mm/2, CellSizeXY_mm/2, CalorimeterZ_mm/2);

    G4LogicalVolume* VertLayerLV = new G4LogicalVolume (
        VertLayerS,
        defaultMaterial,
        "VertLayer");
    
    new G4PVReplica(
        "VertLayer",        // its name
        VertLayerLV,        // its logical volume
        CalorimeterLV,      // its mother
        kYAxis,             // axis of replication
        nCellsY ,           // number of replica
        CellSizeXY_mm);     // witdth of replica
     
    //  Horizontal calorimeter layer
    G4VSolid* HorLayerS = new G4Box (
        "HorLayer",
        CellSizeXY_mm/2, CellSizeXY_mm/2, CalorimeterZ_mm/2);
    
    G4LogicalVolume* HorLayerLV = new G4LogicalVolume (
        HorLayerS,
        defaultMaterial,
        "HorLayer");
    
    new G4PVReplica(
        "HorLayer",
        HorLayerLV,
        VertLayerLV,
        kXAxis,
        nCellsX,
        CellSizeXY_mm);

    //Simple layer: Thickness = Pb + Sc
    G4VSolid* LayerS = new G4Box (
        "Layer",
        CellSizeXY_mm/2, CellSizeXY_mm/2, Layer_mm/2);

    G4LogicalVolume* LayerLV = new G4LogicalVolume (
        LayerS,
        defaultMaterial,
        "Layer");
    
    new G4PVReplica(
        "Layer",
        LayerLV,
        HorLayerLV,
        kZAxis,
        NLayers,
        Layer_mm);

    //  Pb layer
    G4VSolid* PbS = new G4Box (
        "Pb",
        CellSizeXY_mm/2, CellSizeXY_mm/2, Pb_mm/2);

    G4LogicalVolume* PbLV = new G4LogicalVolume (
        PbS,
        AbsorberMaterial,
        "Pb");
    
    new G4PVPlacement (
        0,
        G4ThreeVector(0., 0., -Sc_mm/2),
        PbLV,
        "Pb",
        LayerLV,
        false,
        0,
        CheckOverlaps);
    
    //  Sc Layer
    G4VSolid* ScS = new G4Box (
        "Sc",
        CellSizeXY_mm/2, CellSizeXY_mm/2, Sc_mm/2);

    G4LogicalVolume* ScLV = new G4LogicalVolume (
        ScS,
        ScMaterial,
        "Sc");
    
    new G4PVPlacement (
        0,
        G4ThreeVector(0., 0., Pb_mm/2),
        ScLV,
        "Sc",
        LayerLV,
        false,
        0,
        CheckOverlaps);
    
    
        
    //                                        
    //  Visualization attributes
    G4VisAttributes* simpleBoxVisAtt= new G4VisAttributes(G4Colour(1.0,1.0,1.0));
    G4VisAttributes* gray = new G4VisAttributes (G4Colour(0.9,0.9,0.9)); 
    //
    //WorldLV->SetVisAttributes (G4VisAttributes::Invisible);
    WorldLV->SetVisAttributes (simpleBoxVisAtt);

    simpleBoxVisAtt->SetVisibility(true);
    CalorimeterLV->SetVisAttributes(simpleBoxVisAtt);
    VertLayerLV->SetVisAttributes(simpleBoxVisAtt);  
    HorLayerLV->SetVisAttributes(simpleBoxVisAtt);
    MagneticLV->SetVisAttributes(gray);  
    
    //LayerLV->SetVisAttributes(simpleBoxVisAtt);   //  TODO  Do the normal way
    //PbLV->SetVisAttributes(simpleBoxVisAtt);      //        to change VisSettings
    //ScLV->SetVisAttributes(simpleBoxVisAtt);      //        Recompile the project everytime is a stupid idea
   
    LayerLV->SetVisAttributes(G4VisAttributes::Invisible);   
    PbLV->SetVisAttributes(G4VisAttributes::Invisible);    
    ScLV->SetVisAttributes(G4VisAttributes::Invisible);     

    // Always return the physical World
    //
    return WorldPV;

}


void SRdetDetectorConstruction::ConstructSDandField() {
       
    SRdetCalorimeterSD* ScSD = new SRdetCalorimeterSD( "ScintillatorSD", "ScintillatorHitsCollection" );
    SetSensitiveDetector ("Sc", ScSD);
    
    //  Create uniform magnetic field----------------------------------------------------------------------
    MagField = new MagneticField();
       
    //  Create field manager and set default field
    FieldMan = new G4FieldManager();
    FieldMan->SetDetectorField(MagField);
    
    //  Create the object which calculate the trajectory
    FieldMan->CreateChordFinder(MagField);
    
    //  Assosiate with logical volume
    G4bool ToAllDaughters = true;
    MagneticLV->SetFieldManager(FieldMan, ToAllDaughters);
    
    // Register the field messenger and field for deleting
    G4AutoDelete::Register(MagField);
    G4AutoDelete::Register(FieldMan);

}



    
