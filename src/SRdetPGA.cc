#include "SRdetPGA.hh"
#include "SRdetUSets.hh"

#include "G4LogicalVolume.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4Box.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"

#include "Randomize.hh"

SRdetPGA::SRdetPGA() : G4VUserPrimaryGeneratorAction()
{
    G4int NParticles = 1;
    PGun = new G4ParticleGun( NParticles );

    IsPos = false;
    
    //  default particle kinematic

    G4ParticleDefinition* PDefinition = G4ParticleTable::GetParticleTable()->FindParticle("e-");
    PGun->SetParticleDefinition( PDefinition );
    PGun->SetParticleMomentumDirection( G4ThreeVector(0.,0.,1.) );
    PGun->SetParticleEnergy( SRdetUSets::Instance()->GetSREnergy()*MeV );

}

SRdetPGA::~SRdetPGA()
{
    delete PGun;
}

void SRdetPGA::GeneratePrimaries( G4Event* tEvent )
{
    G4String tBeamType = SRdetUSets::Instance()->GetBeamType();
    if ( tBeamType.compare("noDistribution") == 0 )
    {
        //  Gun position will be set once per event in this case
        if ( IsPos == false) 
        {
            IsPos = true;
            PGun->SetParticlePosition( G4ThreeVector(0, 0, -SRdetUSets::Instance()->GetPGunPos()) );
        }
    }
    //  TODO add Gauss distribution
    else 
    {
        G4double zbeam = -SRdetUSets::Instance()->GetPGunPos();
	    G4double xbeam = 0. + 2.*cm * ( G4UniformRand() - 0.5 );
	    G4double ybeam = 0. + 2.*cm * ( G4UniformRand() - 0.5 );
	    PGun->SetParticlePosition( G4ThreeVector( xbeam, ybeam, zbeam ) );  
    } 
           
    PGun->GeneratePrimaryVertex( tEvent );
}





