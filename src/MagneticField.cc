#include "MagneticField.hh"

#include "G4SystemOfUnits.hh"
#include "globals.hh"

MagneticField::MagneticField()
: G4MagneticField(), MagneticValue(1.43*tesla)
{}

MagneticField::~MagneticField()
{}

void MagneticField::GetFieldValue(const G4double [4], double *bField) const
{
    bField[0] = 0;
    bField[1] = -MagneticValue;
    bField[2] = 0;
}
