#include "SRdetActionInitialization.hh"
#include "SRdetRunAction.hh"
#include "SRdetEventAction.hh"
#include "SteppingAction.hh"

#include "SRdetPGA.hh"

SRdetActionInitialization::SRdetActionInitialization()
    : G4VUserActionInitialization()
{
}

SRdetActionInitialization::~SRdetActionInitialization()
{
}

//  TODO  SRdetActionInitialization::BuiltForMaster()
//  {
//      SetUserAction(new SRdetRunAction);
//  }

void SRdetActionInitialization::Build() const
{
    SetUserAction(new SRdetPGA());
    SetUserAction(new SRdetRunAction());
    SetUserAction(new SRdetEventAction());
    SetUserAction(new SteppingAction());
}
