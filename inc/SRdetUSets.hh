#ifndef SRdetUSets_h
#define SRdetUSets_h 1

#include "globals.hh"

class SRdetUSets
{
    protected :
        static SRdetUSets* _self;
        SRdetUSets();
        virtual ~SRdetUSets();

        G4double PbThickness_mm;
        G4double SREnergy_mev;
        G4int NLayers;
        
        G4double Xpos;  //  X coord of Shashliks center
        
        G4int NofCellsX;
        G4int NofCellsY; 
        
        G4double PGunPos;

        G4String BeamType;
    public :
        static SRdetUSets* Instance();

        void SetPbThickness( G4double );
        void SetSREnergy( G4double );
        void SetNLayers( G4int );
        void SetPGunPos( G4double );        
       
        void SetXpos( G4double );  //  X coordinate for Shaslik's center
        
        void SetNofCellsX( G4int );
        void SetNofCellsY( G4int );

        void SetBeamType( G4String );
            
        void PrintUsage();
    
        G4double GetPbThickness() { return PbThickness_mm; }
        G4double GetSREnergy() { return SREnergy_mev; }
        G4int GetNLayers() { return NLayers; }
        G4double GetPGunPos() { return PGunPos;}
        G4double GetXpos() { return Xpos;}
        
        G4int GetNofCellsX() { return NofCellsX; }
        G4int GetNofCellsY() { return NofCellsY; }
        G4String GetBeamType() { return BeamType; }
};

#endif
