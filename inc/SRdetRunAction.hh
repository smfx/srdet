#ifndef SRdetRunAction_h
#define SRdetRunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"

class HistoMan;

class SRdetRunAction : public G4UserRunAction
{
    public :
        SRdetRunAction();
        virtual ~SRdetRunAction();
    
        virtual void BeginOfRunAction(const G4Run*);
        virtual void EndOfRunAction(const G4Run*);
    private:
        HistoMan* hMan;
};
#endif
