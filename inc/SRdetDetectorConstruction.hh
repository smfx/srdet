#ifndef SRdetDetectorConstruction_h
#define SRdetDetectorConstructio_h 1

#include "G4VUserDetectorConstruction.hh"
#include "G4FieldManager.hh"

#include "globals.hh"

class G4VPhysicalVolume;
class MagneticField;

class SRdetDetectorConstruction : public G4VUserDetectorConstruction {
    
    public:
        SRdetDetectorConstruction();
        virtual ~SRdetDetectorConstruction();
        
    public:
        virtual G4VPhysicalVolume* Construct();
        virtual void ConstructSDandField();
                
    private:
        
        G4LogicalVolume* MagneticLV;
        
        static G4ThreadLocal G4FieldManager* FieldMan;
        static G4ThreadLocal MagneticField* MagField; 

        void DefineMaterials();
        G4VPhysicalVolume* DefineVolumes();

        G4bool CheckOverlaps;
        
};

#endif
