#ifndef HistoMan_h 
#define HistoMan_h 1

#include "globals.hh"

#include "g4root.hh"

class HistoMan
{
    public:
        HistoMan();
        ~HistoMan();

    private:
        void Book();
        G4String FileName;
};
#endif
