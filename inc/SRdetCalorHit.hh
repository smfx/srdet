#ifndef SRdetCalorHit_h
#define SRdetCalorHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4ThreeVector.hh"
#include "tls.hh"

class SRdetCalorHit : public G4VHit
{
    public:
        SRdetCalorHit();
        SRdetCalorHit( const SRdetCalorHit& );
        virtual ~SRdetCalorHit();
        
        //  overloading operators
        const SRdetCalorHit& operator=(const SRdetCalorHit&);
        G4int operator==(const SRdetCalorHit&) const;
        
        //  methods from the base class
        virtual void Draw() {}
        virtual void Print();

        //  methods for handling data
        void Add(G4double de);

        //  get methods
        G4double GetEdep() const;
            
    private:
        G4double Edep;
               
};

typedef G4THitsCollection<SRdetCalorHit> SRdetCalHitsCollection;

inline void SRdetCalorHit::Add(G4double de)
{
    Edep += de;
}

inline G4double SRdetCalorHit::GetEdep() const 
{
    return Edep;
}

#endif
