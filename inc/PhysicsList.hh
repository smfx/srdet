#ifndef PhysicsList_h
#define PhysicsList_h 1

#include "G4VUserPhysicsList.hh"
#include "globals.hh"

//  TODO  class PhysicsListMessenger;
class G4SynchrotronRadiation;

class PhysicsList : public G4VUserPhysicsList
{
    public :
        PhysicsList();
        ~PhysicsList();
    
        virtual void ConstructParticle();
        virtual void ConstructProcess();

        inline void SetAnalyticSR (G4bool tValue) {SRType = tValue;};

    private :
        void ConstructBosons();
        void ConstructLeptons();

        void ConstructGeneral();
        void ConstructEM();

        G4bool SRType;
        //  TODO  PhysListMessenger* PhysListMesgr;
};
#endif
