#ifndef SRdetPGA_h
#define SRdetPGA_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "globals.hh"

class G4ParticleGun;
class G4Event;

/// The primary generator action class with particle gum.
///
/// It defines a single particle which hits the Tracker 
/// perpendicular to the input face. The type of the particle
/// can be changed via the G4 build-in commands of G4ParticleGun class 
/// (see the macros provided with this example).

class SRdetPGA : public G4VUserPrimaryGeneratorAction
{
    public :
        SRdetPGA();
        virtual ~SRdetPGA();

        virtual void GeneratePrimaries(G4Event* );
    
        G4ParticleGun* GetPGun() { return PGun; }
        void SetRandomFlag(G4bool );
    
    private :
        G4bool IsPos;
        G4ParticleGun* PGun;  //  Particle Gun
        
};

#endif
