#ifndef MagneticField_h
#define MagneticField_h 1

#include "globals.hh"
#include "G4MagneticField.hh"

class MagneticField : public G4MagneticField
{
    public:
        MagneticField();
        virtual ~MagneticField();

        virtual void GetFieldValue(const G4double point[4], double* bField) const;

        void SetField(G4double tValue) { MagneticValue = tValue;}
        G4double GetField() const {return MagneticValue;}

    private:
        G4double MagneticValue;
};
#endif
