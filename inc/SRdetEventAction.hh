#ifndef SRdetEventAction_h
#define SRdetEventAction_h 1

#include "G4UserEventAction.hh"

#include "SRdetCalorHit.hh"

#include "globals.hh"

class SRdetEventAction : public G4UserEventAction
{
    public :
        SRdetEventAction();
        virtual ~SRdetEventAction();

        virtual void BeginOfEventAction( const G4Event* event );
        virtual void EndOfEventAction( const G4Event* event );
    private :
        //  methods
        SRdetCalHitsCollection* GetHitsCollection( G4int hcID, const G4Event* event) const;
        void PrintEventStatistics(G4double ScEdep) const;
        
        //  data members
        G4int ScHCEID;
};

#endif
