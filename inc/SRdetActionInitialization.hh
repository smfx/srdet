#ifndef SRdetActionInitialization_h
#define SRdetActionInitialization_h 1

#include "G4VUserActionInitialization.hh"

class SRdetDetectorConstruction;

class SRdetActionInitialization : public G4VUserActionInitialization 
{
    public:
        SRdetActionInitialization();
        virtual ~SRdetActionInitialization();
    
        //  virtual void BuildForMaster() const;
        virtual void Build() const;
};

#endif
